package classes;

public class Main {

    public static void main(String[] args) {

        // Classes are how we build our own custom objects. They are usually modeled to have a similar shape to the underlying business data of our application

        // Let's use a Pet as an example. We may be building a record keeper for a Vet's office and they have tons of data on different pets. How could we model this data? Look at classes/Pet.java to see

        // Let's use the Pet constructor to create a new example dog:
        Pet examplePet = new Pet("dog", "Bernie", "Paul");

        // Let's take a look at the Pet's different properties:
        // System.out.println(examplePet.name);
        // System.out.println(examplePet.type);
        // System.out.println(examplePet.owner);

        // Let's take a look at the Pet's different properties using the appropriate getters()
        // System.out.println(examplePet.getName());
        // System.out.println(examplePet.getType());
        // System.out.println(examplePet.getOwner());

        // Let's use a setter to update the name of this example pet
        // examplePet.setName("Zelda");
        // System.out.println(examplePet.getName());

        // Let's call the Pet's talk() method
        // System.out.println(examplePet.talk());
    }
    
}
