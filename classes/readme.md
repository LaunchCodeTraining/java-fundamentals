# Classes and Objects in Java

This covers:

- creating classes
- class properties
- class constructor
- getters and setters
- class methods
- instantiating objects from classes
- accessing properties on objects
- updating properties on objects
- calling methods on objects