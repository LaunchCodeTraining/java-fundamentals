package classes;

// We need to first define our file as a class

public class Pet {

    // We then declare the properties associated with our Pet objects
    private String type; // the type of pet, most commonly dog, or cat
    private String name; // the name of the pet
    private String owner; // the name of the owner of the pet

    // Our constructors -- the methods that allow us to create a new Pet object
    // In this case we want to declare a new Pet object when the user can provide all of the pre-defined properties
    Pet(String petType, String petName, String petOwner) {
        this.type = petType;
        this.name = petName;
        this.owner = petOwner;
    }

    // Our property access controllers (getters and setters)
    public String getType() {
        return this.type;
    }

    public String getName() {
        return this.name;
    }

    public String getOwner() {
        return this.owner;
    }

    public void setType(String petType) {
        this.type = petType;
    }

    public void setName(String petName) {
        this.name = petName;
    }

    public void setOwner(String petOwner) {
        this.owner = petOwner;
    }

    // associated Object methods

    public String talk() {
        if(this.type == "dog") {
            return "Woof!";
        }
        else if(this.type == "Cat") {
            return "Meow!";
        }
        return "...";
    }
}
