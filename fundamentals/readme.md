# Programming Fundamentals in Java

This covers:

- basic Java information
- variables and data types
- conditional statements
- looping statements
- static methods
- Fizzbuzz in Java