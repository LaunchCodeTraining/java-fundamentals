package fundamentals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Example {

    public static void main(String[] args) {

        /***** Java *****/

        // Java is a compiled, strictly typed, object oriented programming language

        /** Compiled **/

        // To run our Java code we must first compile our code into Java bytecode. This bytecode is what the JVM can run.

        // So to run the java code we write, we must first compile it using this file as an example from variables/:

        // $ javac variables/Example.java -- this generates a Example.class file which is our Java bytecode.

        // We can then use the JVM to run this code with:

        // $ java variables.Example

        // Alternatively, with the proper VSC extensions, you can simply click "Run" above the Main method -- if you are interested in how VSC does this, take a look at the command that is run from the VSC Java Process Console

        // Today we will be running Java from the terminal, or VSC tomorrow we will be running our java projects using the build tool Gradle, since we will begin working with dependencies

        /** Strictly Typed **/

        // In Java you MUST declare the type of variable while declaring it

        // In JavaScript (loosely typed), we were able to simply say `let` or `const` and then follow it with whatever value we want and JavaScript figures out the type for us in the background

        // In Java your code will NOT compile if it cannot determine the type of a variable. Luckily with the proper extensions most IDEs (including VSC) will let you know when you have forgotten to declare the type of a variable

        // Places to look out for type:
        // Declaring Variables
        // In loops
        // In method signatures

        /** Type Casting and Parsing **/

        // After declaring a type for a variable, you cannot change the type of that variable (from int x = 15; to x = 15.4)

        // If you need to change the type for some reason you will need to cast or parse the variable from one type to another

        /** Heavily Object Oriented **/

        // *Almost* everything is an object in Java -- Take a look at the top of this file

        // To run a simple Java program to illustrate programming fundamentals we had to declare a Class (the Example Class). Inside of this class we had to attach a method, the Main method which kicks off our application.

        // Working with Java usually requires working with many Classes and Objects, and in many cases defining your own custom classes and objects -- We will look at this in greater depth in a different file

        /****  Variables ****/

        /* variableType variableName = value; */
        int integerVariable = 0;
        // System.out.println(integerVariable);

        /****  Data Types - Primitives ****/

        /** integer **/
        int integerExample = 15;
        // System.out.println(integerExample);

        /** double **/
        double doubleExample = 15.5;
        // System.out.println(doubleExample);

        /** boolean **/
        boolean booleanExample = false;
        // System.out.println(booleanExample);

        /** character **/
        char charExample = 'b';
        // System.out.println(charExample);

        /** Casting Example **/
        // Changing an integer into a double
        int castingExample = 5;
        // System.out.println(castingExample);
        // System.out.println((double) castingExample);

        /**** Data Types - Non Primitive ****/

        /** String -- collection of *string* characters **/
        String stringExample = "This is an example string";
        // System.out.println(stringExample);

        /** Splitting Strings **/

        /* split() */
        // System.out.println(Arrays.toString(stringExample.split(" ")));

        /** Array -- static collection of one data type **/
        int[] intArrayExampleOne = {1,2,3,4,5};
        // System.out.println(Arrays.toString(intArrayExampleOne));
        // System.out.println(intArrayExampleOne.toString());
        int[] intArrayExampleTwo = new int[5];
        // System.out.println(Arrays.toString(intArrayExampleTwo));

        /* Accessing elements in an array */
        intArrayExampleTwo[0] = 5;
        intArrayExampleTwo[1] += 3;
        // System.out.println(Arrays.toString(intArrayExampleTwo));

        /** ArrayList -- dynamic collection of one data type **/
        ArrayList<Integer> intArrayListExample = new ArrayList<Integer>();
        // System.out.println(intArrayListExample.toString());

        /* Adding elements to an ArrayList */
        intArrayListExample.add(15);
        intArrayListExample.add(25);
        // System.out.println(intArrayListExample.toString());

        /* Removing elements from an ArrayList */
        intArrayListExample.remove(0);
        // System.out.println(intArrayListExample.toString());

        /** HashMap -- collection of key value pairs **/
        HashMap<String, String> exampleHashMap = new HashMap<String, String>();
        // System.out.println(exampleHashMap);

        /* adding new key value pairs to a HashMap */
        exampleHashMap.put("someKey", "someValue");
        // System.out.println(exampleHashMap);

        /* accessing all of the keys in a HashMap */
        // System.out.println(exampleHashMap.keySet());

        /* checking if a certain key exists in a HashMap */
        // System.out.println(exampleHashMap.containsKey("someKey"));

        /**** Conditionals ****/

        /* If */
        boolean booleanForConditional = true;
        if(booleanForConditional) {
            // System.out.println("condition was true!");
        }

        /* If/else */
        booleanForConditional = false;
        if(booleanForConditional) {
            // System.out.println("condition was false");
        }
        else {
            // System.out.println("the condition was not false");
        }

        /* If/else if/else */
        int someValueForConditional = 2;
        if(someValueForConditional == 0) {
            // System.out.println("value was 0");
        }
        else if (someValueForConditional == 1) {
            // System.out.println("value was 1");
        }
        else if(someValueForConditional == 2) {
            // System.out.println("value was 2");
        }

        /**** loops ****/

        /** while **/

        int whileCounter = 0;
        while(whileCounter < 5) {
            whileCounter += 1;
            // System.out.println(whileCounter);
        }

        /** for **/
        for(int i = 0; i < 7; i++) {
            // System.out.println(i);
        }

        /** foreach **/
        int[] forArray = {7,6,5,4,3};
        for(int i : forArray) {
            // System.out.println(i);
        }

        /**** Functions? ****/
        someMethod();

        /** Method with Return **/
        int returnedValue = someMethodWithReturn();
        // System.out.println(returnedValue);

        /** Method with Arguments **/
        double someDoubleForMethodExample = 25.5;
        someMethodWithArguments(15, someDoubleForMethodExample);

        /** Let's write FizzBuzz together! */
        // fizzBuzz(100);
    }

    public static void someMethod() {
        // System.out.println("Since every method HAS to be attached to a class in Java, they are all technically methods!");
        // System.out.println("....isn't pedantry great!?");
    }

    public static int someMethodWithReturn() {
        return 5;
    }

    public static void someMethodWithArguments(int firstArgument, double secondArgument) {
        // System.out.println(firstArgument);
        // System.out.println(secondArgument);
    }

    public static void fizzBuzz(int fizzBuzzLimit) {
        for(int i = 1; i <= fizzBuzzLimit; i++) {
            if(i % 15 == 0) {
                System.out.println("FIZZBUZZ");
            }
            else if(i % 3 == 0) {
                System.out.println("fizz");
            }
            else if(i % 5 == 0) {
                System.out.println("buzz");
            }
            else {
                System.out.println(i);
            }
        }
    }

}
